cmake_minimum_required(VERSION 3.5)
project(XmlObject LANGUAGES CXX VERSION 1.0)
enable_testing()
option (BUILD_DOC "documentation build" ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
#setup output directory for bin and tests
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin )
#setup output directory for libs
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib )
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib)
#qt
find_package(Qt5 COMPONENTS Core Xml REQUIRED)
#doxygen
if( ${CMAKE_PROJECT_NAME} STREQUAL "XmlObject" )
find_package( Doxygen )
if( DOXYGEN_FOUND)
	set( DOXYGEN_PROJECT_NAME XmlObject )
	set( DOXYGEN_PROJECT_VERSION ${CMAKE_PROJECT_VERSION} )
	set( DOXYGEN_GENERATE_HTML YES )
	set( DOXYGEN_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/doc/)
	set( DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/build/CMakeDoxyfile.in)
	set( DOXYGEN_INPUT ${CMAKE_SOURCE_DIR}/include/)
	set( DOXYGEN_OUT ${CMAKE_SOURCE_DIR}/doc/Doxyfile )
	configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
	add_custom_target( XmlObjectDoc ALL
	COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
	WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
	COMMENT "Generating API documentation with Doxygen"
	VERBATIM )
	doxygen_add_docs(doxygen ${PROJECT_SOURCE_DIR} COMMENT "documentation generated" )
else()
  message( "doxygen package not found" )
endif( DOXYGEN_FOUND )
endif()
add_library(XmlObject SHARED
  include/XmlObject_global.h
  include/XmlObject.h
  include/XmlRegister.h
  src/XmlObject.cpp
)

target_link_libraries(XmlObject PRIVATE Qt5::Core Qt5::Xml)

target_compile_definitions(XmlObject PRIVATE XMLOBJECT_LIBRARY)

#ifndef XMLREGISTER_H
#define XMLREGISTER_H
#include "XmlObject_global.h"
#include <QString>
#include <QVector>
#include <typeinfo>

#define REGISTER_XML( type ) XmlRegister::object()->registerType( qMetaTypeId< type >() );
/**
 * @brief The XmlRegister class. Helper class to register object.
 */
class XMLOBJECT_EXPORT XmlRegister
{
private:
	XmlRegister() = default;
	QVector< int > _register;

public:
	/**
	 * @brief Singleton implementation.
	 * @return
	 */
	inline static XmlRegister* object()
	{
		static XmlRegister* obj = new XmlRegister;
		return obj;
	}
	/**
	 * @brief Function to register type by it number. Function will return true if object is not yet registerd, in other hand false will be returned.
	 * @param info number type.
	 * @return
	 */
	inline bool registerType( int info )
	{
		bool out = false;
		if ( !_register.contains( info ) )
		{
			out = true;
			_register.append( info );
		}
		return out;
	}
	/**
	 * @brief Function to check if type represented by this number is regitered.
	 * @param type
	 * @return
	 */
	inline bool isRegistered( int type )
	{
		return _register.contains( type );
	}
	/**
	 * @brief Function return registerd types.
	 * @return
	 */
	inline QVector< int > registeredTypes()
	{
		return _register;
	}
};

#endif // XMLREGISTER_H

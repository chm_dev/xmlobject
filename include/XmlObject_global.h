#ifndef XMLOBJECT_GLOBAL_H
#define XMLOBJECT_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined( XMLOBJECT_LIBRARY )
#define XMLOBJECT_EXPORT Q_DECL_EXPORT
#else
#define XMLOBJECT_EXPORT Q_DECL_IMPORT
#endif

#endif // XMLOBJECT_GLOBAL_H

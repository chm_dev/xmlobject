#ifndef XMLOBJECT_H
#define XMLOBJECT_H
#include "XmlObject_global.h"
#include "XmlRegister.h"
#include <QDebug>
#include <QMetaObject>
#include <QMetaProperty>
#include <QObject>
#include <QStringList>
#include <QtXml/QDomAttr>
#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>
#include <memory>

#define PROPERTY( type, name, read, write, notify )                \
	Q_PROPERTY( type _##name READ read WRITE write NOTIFY notify ) \
private:                                                           \
	type _##name;                                                  \
																   \
public:                                                            \
	type read() const                                              \
	{                                                              \
		return _##name;                                            \
	}                                                              \
Q_SIGNALS:                                                         \
	void notify( type name );                                      \
public slots:                                                      \
	void write( const type& val )                                  \
	{                                                              \
		if ( _##name != val )                                      \
		{                                                          \
			_##name = val;                                         \
			emit notify( val );                                    \
		}                                                          \
	}

#define PRIVATE_PROPERTY( type, name, read, write, notify )        \
	Q_PROPERTY( type _##name READ read WRITE write NOTIFY notify ) \
private:                                                           \
	type _##name;                                                  \
																   \
public:                                                            \
	type read() const                                              \
	{                                                              \
		return _##name;                                            \
	}                                                              \
Q_SIGNALS:                                                         \
	void notify( type name );                                      \
private slots:                                                     \
	void write( const type& val )                                  \
	{                                                              \
		if ( _##name != val )                                      \
		{                                                          \
			_##name = val;                                         \
			emit notify( val );                                    \
		}                                                          \
	}

#define SIGNAL_LESS_PROPERTY( type, name, read, write ) \
	Q_PROPERTY( type _##name READ read WRITE write )    \
private:                                                \
	type _##name;                                       \
														\
public:                                                 \
	type read() const                                   \
	{                                                   \
		return _##name;                                 \
	}                                                   \
public slots:                                           \
	void write( const type& val )                       \
	{                                                   \
		_##name = val;                                  \
	}

#define SIGNAL_LESS_POINTER_PROPERTY( type, name, read, write ) \
	Q_PROPERTY( type _##name READ read WRITE write )            \
private:                                                        \
	type _##name;                                               \
																\
public:                                                         \
	type read() const                                           \
	{                                                           \
		return _##name;                                         \
	}                                                           \
public slots:                                                   \
	void write( type val )                                      \
	{                                                           \
		_##name = val;                                          \
	}
/**
 * @brief Class XmlObject is base to create simple structs. Created struct should contains only POD types members and interface to get and set property value. To create struct, this class use Q_PROPERTY mechanism. It also allow to serialization and deserialization from and to xml and QByteArray.
 */
class XMLOBJECT_EXPORT XmlObject : public QObject
{
	Q_OBJECT
public:
	/**
	 * @brief Constructor, It will create empty QObject. It will set, passed in argument QObject, as a it parent.
	 * @param parent
	 */
	explicit XmlObject( QObject* parent = nullptr );
	/**
	 * @brief Method, will parse passed in argument xml and set all available object properties to values read from xml.
	 * @param xml structure must be equivaled of object which call this function.
	 */
	virtual void fromXml( const QString& xml );
	/**
	 * @brief Funciton transform object to xml. If object is part of biger whole, additional argument as: document and element must be passed.
	 * @param document QDomDocument to which, transformed to xml element can be added.
	 * @param parent QDomElement to which, transformed to xml element can be added.
	 * @return xml in QString format.
	 */
	QString toXml( QDomDocument document = QDomDocument(), QDomElement parent = QDomElement() );
	/**
	 * @brief Overloaded function to transform object to xml. Created object will be added to QDomDocument, property count will be cut to only this which will passed in argument as properties list.
	 * @param propertyList
	 * @param document
	 */
	void toXml( QStringList propertyList, QDomDocument& document );
	/**
	 * @brief Method check if, passed in argument, xml is valid, to set properties object which call function. If xml structure is correct method return true on other hand it return false.
	 * @param xml
	 * @return
	 */
	bool isValid( const QString& xml );
	/**
	 * @brief Function transform object to QByteArray representation.
	 * @return
	 */
	QByteArray toByteArray();
	/**
	 * @brief Overloaded function to transform list of pointers to xml. If object is part of biger whole, additional argument as: document and element must be passed.
	 * @param list
	 * @param document QDomDocument to which, transformed to xml element can be added.
	 * @param parent QDomElement to which, transformed to xml element can be added.
	 * @return xml in QString format
	 */
	template < class T >
	QString toXml( QList< T* >* list, bool /*isRoot*/, QDomDocument document = QDomDocument(), QDomElement parent = QDomElement() )
	{
		QString out;
		QString parentName  = parent.tagName();
		QDomElement element = parent;
		if ( parentName.isEmpty() )
		{
			parent = document.createElement( metaObject()->className() );
			document.appendChild( parent );
			element = parent;
		}
		for ( int o = 0; o < list->size(); ++o )
		{
			list->at( o )->toXml( document, element );
		}
		out += document.toString();
		return out;
	}
	/**
	 * @brief Overloaded function to transform vector of pointers to xml. If object is part of biger whole, additional argument as: document and element must be passed.
	 * @param vector
	 * @param document QDomDocument to which, transformed to xml element can be added.
	 * @param parent QDomElement to which, transformed to xml element can be added.
	 * @return xml in QString format
	 */
	template < class T >
	QString toXml( QVector< T* >* vector, bool /*isRoot*/, QDomDocument document = QDomDocument(), QDomElement parent = QDomElement() )
	{
		QString out;
		QString parentName = parent.tagName();
		if ( parentName.isEmpty() )
		{
			parent = document.createElement( metaObject()->className() );
			document.appendChild( parent );
		}
		for ( int o = 0; o < vector->size(); ++o )
		{
			if ( vector->at( o ) != nullptr )
			{
				vector->at( o )->toXml( document, parent );
			}
		}
		out += document.toString();
		return out;
	}
	/**
	 * @brief toXml Overloaded function to transfomrm object to xml. Function add also additional properties from hash passed in argument.
	 * @param hash with additional properties.
	 * @param document QDomDocument to which, transformed to xml element can be added.
	 * @param parent QDomElement to which, transformed to xml element can be added.
	 * @return
	 */
	QString
	toXml( QHash< QString, QVariant > hash, bool /*isRoot*/, QDomDocument document = QDomDocument(), QDomElement parent = QDomElement() )
	{
		QString out;
		QString parentName  = parent.tagName();
		QDomElement element = parent;
		if ( parentName.isEmpty() )
		{
			parent = document.createElement( metaObject()->className() );
			document.appendChild( parent );
			element = parent;
		}
		QHash< QString, QVariant >::iterator i;
		for ( i = hash.begin(); i != hash.end(); ++i )
		{
			element.setAttribute( i.key(), i.value().toString() );
		}
		out += document.toString();
		return out;
	}
	/**
	 *@brief Function to transform xml structure to list of pointers.
	 *@param xml
	 */
	template < class T > static QList< T* > toList( QString xml )
	{
		QList< T* > outList;
		QDomDocument doc;
		doc.setContent( xml );
		T tmp;
		QDomNodeList list = doc.elementsByTagName( tmp.metaObject()->className() );
		for ( int i = 0; i < list.size(); ++i )
		{
			T* o                = new T;
			QDomElement element = list.at( i ).toElement();
			for ( int p = 0; p < o->metaObject()->propertyCount(); ++p )
			{
				if ( o->metaObject()->property( p ).isScriptable() )
				{
					QVariant value = QVariant( element.attributeNode( o->metaObject()->property( p ).name() ).value() );
					o->metaObject()->property( p ).write( o, value );
				}
			}
			outList << o;
		}
		return outList;
	}
	/**
	 * @brief Overloaded function to transform list of pointer to xml.
	 * @param list
	*/
	template < class T > static QString toXml( QList< T* > list )
	{
		QString out;
		QDomDocument doc;
		QDomElement root = doc.createElement( "root" );
		doc.appendChild( root );
		for ( int o = 0; o < list.size(); ++o )
		{
			list[o]->toXml( doc, root );
		}
		out += doc.toString();
		return out;
	}
	/**
	 * @brief Overloaded function to transform vector of pointer to xml.
	 * @param vector
	*/
	template < class T > static QString toXml( QVector< T* > vector )
	{
		QString out;
		QDomDocument doc;
		QDomElement root = doc.createElement( "root" );
		doc.appendChild( root );
		for ( int o = 0; o < vector.size(); ++o )
		{
			vector[o]->toXml( doc, root );
		}
		out += doc.toString();
		return out;
	}
	/**
@brief Function add additional property to object.
@param name property name
@param value property value
*/
	Q_INVOKABLE inline bool setProperty( const char* name, const QVariant& value )
	{
		return QObject::setProperty( name, value );
	}

signals:
	/**
	 * @brief Signal is emited when some error in transfer between different forms of object occure.
	 * @param error
	 */
	void errorOccure( const QString& error );
};
template < typename T > static const char* typeName( T /*type*/ )
{
	return typeid( T ).name();
}
typedef QHash< QString, QVariant > SVHash;
typedef QHash< QString, QString > SSHash;
typedef QHash< QString, XmlObject* > XmlObjectHash;
typedef QHash< QString, std::shared_ptr< XmlObject > > SharedPtrXmlObjectHash;
using SPXOVector = QVector< std::shared_ptr< XmlObject > >;
template < typename T > class XmlObjectsList : public QList< T >
{
};
template < typename T > class XmlObjectsVector : public QVector< T >
{
};
Q_DECLARE_METATYPE( XmlObjectsList< XmlObject* > )
Q_DECLARE_METATYPE( XmlObjectsVector< XmlObject* > )
Q_DECLARE_METATYPE( SVHash )
Q_DECLARE_METATYPE( SSHash )

#endif // XMLOBJECT_H

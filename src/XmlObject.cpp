#include "include/XmlObject.h"
#include <QDebug>
#include <QMetaType>
#include <type_traits>
XmlObject::XmlObject( QObject* parent )
	: QObject( parent )
{
}

void XmlObject::fromXml( const QString& xml )
{
	QDomDocument doc;
	doc.setContent( xml );
	QDomNodeList list = doc.elementsByTagName( metaObject()->className() );
	if ( !list.isEmpty() )
	{

		if ( list.size() > 1 )
		{
			emit errorOccure( QString( "Xml has more then one instance of %0 class" ).arg( metaObject()->className() ) );
		}
		else
		{
			if ( list.at( 0 ).isElement() )
			{
				QDomElement element = list.at( 0 ).toElement();
				for ( int i = 0; i < metaObject()->propertyCount(); ++i )
				{
					if ( metaObject()->property( i ).isScriptable() )
					{
						auto name        = metaObject()->property( i ).name();
						auto type        = metaObject()->property( i ).type();
						auto value       = metaObject()->property( i ).read( this );
						QString typeName = value.typeName();
						if ( type != QVariant::UserType )
						{
							if ( type == QMetaType::QVariantList )
							{
								QStringList str = element.attributeNode( name ).value().split( "," );
								QVariantList list;
								list.reserve( str.size() );
								for ( const auto& i : str )
								{
									list << QVariant( i );
								}
								metaObject()->property( i ).write( this, list );
							}
							else
							{
								metaObject()->property( i ).write( this, QVariant( element.attributeNode( name ).value() ) );
							}
						}
						else if ( XmlRegister::object()->isRegistered( QMetaType::type( qPrintable( typeName ) ) ) )
						{
							auto pointerProperty = qvariant_cast< XmlObject* >( value );
							pointerProperty->fromXml( xml );
						}
						else
						{
							qWarning() << "This object has property which will not be parsed automatically by XmlObject";
						}
					}
				}
			}
		}
	}
	else
		emit errorOccure( QString( "Xml doesn't contain class %0" ).arg( metaObject()->className() ) );
}

QString XmlObject::toXml( QDomDocument document, QDomElement parent )
{
	QString out;
	QString parentName = parent.tagName();
	QDomElement element;
	if ( parentName.isEmpty() )
	{
		element = document.createElement( metaObject()->className() );
		document.appendChild( element );
		parent = document.firstChild().toElement();
	}
	else
	{
		element = document.createElement( metaObject()->className() );
		parent.appendChild( element );
	}
	for ( int i = 1; i < metaObject()->propertyCount(); ++i )
	{
		if ( metaObject()->property( i ).isScriptable() )
		{
			QString name        = metaObject()->property( i ).name();
			QVariant value      = metaObject()->property( i ).read( this );
			QVariant::Type type = metaObject()->property( i ).type();
			QString typeName    = value.typeName();
			if ( type == QVariant::UserType )
			{
				if ( typeName.contains( QRegExp( "QVector.*\\*" ) ) )
				{
					QDomElement subElement = document.createElement( name );
					element.appendChild( subElement );
					QVector< XmlObject* >* vector = reinterpret_cast< QVector< XmlObject* >* >( value.data() );
					toXml( vector, false, document, subElement );
				}
				else if ( typeName.contains( QRegExp( "QList.*\\*" ) ) )
				{
					QDomElement subElement = document.createElement( name );
					element.appendChild( subElement );
					QList< XmlObject* >* list = reinterpret_cast< QList< XmlObject* >* >( value.data() );
					toXml( list, false, document, subElement );
				}
				else if ( qvariant_cast< XmlObject* >( value ) )
				{
					QDomElement subElement = document.createElement( name );
					element.appendChild( subElement );
					XmlObject* object = qvariant_cast< XmlObject* >( value );
					object->toXml( document, subElement );
				}
				else
				{
					if ( typeName == QStringLiteral( "QVariantHash" ) )
					{
						QDomElement subElement = document.createElement( name );
						element.appendChild( subElement );
						QHash< QString, QVariant > hash = value.toHash();
						QHash< QString, QVariant >::iterator i;
						for ( i = hash.begin(); i != hash.end(); ++i )
						{
							subElement.setAttribute( i.key(), i.value().toString() );
						}
					}
					else
					{
						QVariant v( value.userType() );
						v = v.fromValue( value.data() );
						element.setAttribute( name, value.toString() );
					}
				}
			}
			else
			{
				if ( type == QMetaType::QVariantList )
				{
					auto list = value.toList();
					QString str;
					int c = 0;
					for ( const auto& i : list )
					{
						str += i.toString();
						if ( c < list.size() - 1 )
						{
							str += ",";
						}
						++c;
					}
					element.setAttribute( name, str );
				}
				else
				{
					element.setAttribute( name, value.toString() );
				}
			}
		}
	}
	out += document.toString();
	return out;
}

void XmlObject::toXml( QStringList propertyList, QDomDocument& document )
{
	QDomElement element = document.createElement( metaObject()->className() );
	for ( int i = 1; i < metaObject()->propertyCount(); ++i )
	{
		if ( propertyList.contains( metaObject()->property( i ).name() ) )
		{
			QString name   = metaObject()->property( i ).name();
			QVariant value = metaObject()->property( i ).read( this );
			element.setAttribute( name, value.toString() );
		}
	}
	document.appendChild( element );
}

bool XmlObject::isValid( const QString& xml )
{
	QDomDocument doc;
	doc.setContent( xml );
	QDomNodeList list = doc.elementsByTagName( metaObject()->className() );
	return ( list.size() == 0 ? false : true );
}

QByteArray XmlObject::toByteArray()
{
	QByteArray data;
	for ( int i = 1; i < metaObject()->propertyCount(); ++i )
	{
		data.append( metaObject()->property( i ).read( this ).toByteArray() );
		data.append( '|' );
	}
	return data;
}
